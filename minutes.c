#include<stdio.h>

int convert(int h, int m)
{
	int res;
	res = h *60 + m;
	return res;
}

int main()
{
	int hours, minutes, result;
	printf("Enter the hours and minutes\n");
	scanf("%d %d", &hours, &minutes);
	result = convert(hours, minutes);
	printf("The number of minutes is %d \n", result);
	return 0;
}