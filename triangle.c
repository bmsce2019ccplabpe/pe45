#include<stdio.h>
#include<math.h>

float inputside()
{
	float side;
	printf("Enter the value of the side\n");
	scanf("%f", &side);
	return side;
}

float computearea(float s1, float s2, float s3)
{
	float s = (s1 + s2 + s3)/2, area;
	if((s>s1) && (s>s2) && (s>s3))
	{
		area = sqrt(s * (s - s1) * (s -s2) * (s - s3));
		return area;
        }
	else
	{
		area = 0;
		return area;
	}	
	return 1;
}

void output(float area)
{
	if(area)
	{
		printf("The area of the triangle is %f", area);
	}
	else
	{
		printf("Invalid Area!\n");
	}
}

int main()
{
	float s1, s2, s3, area;
	s1 = inputside();
	s2 = inputside();
	s3 = inputside();
	area = computearea(s1, s2, s3);
	output(area);
	return 0;
}