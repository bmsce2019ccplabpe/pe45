#include<stdio.h>

void read_array(int arr[][3])
{
	for(int i = 0;i<5;i++)
	{
		printf("Enter the marks of student %d in each subject",i + 1);
		printf("\n");
		for(int j = 0;j<3;j++)
		{
			scanf("%d", &arr[i][j]);		
		}
	}
}	 

void compute(int arr[][3], int max_marks[])
{
	for(int j = 0;j<3;j++)
	{
		int temp = arr[0][j];
		for(int i = 0;i<5;i++)
		{
			if(temp<arr[i][j])
			temp = arr[i][j];
		}
		max_marks[j] = temp;
	}
}

void output(int max_marks[])
{
	for(int i = 0;i<3;i++)
	{
		printf("The maximum marks obtained in subject %d is %d",i + 1,max_marks[i]);
		printf("\n");
	}	
}

int main()
{
	int marks[5][3], max_marks[3];
	read_array(marks);
	compute(marks, max_marks);
	output(max_marks);
	return 0;
}