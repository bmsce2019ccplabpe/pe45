#include<stdio.h>
# define PI 3.14

float input()
{
	float rad1;
	printf("Enter the value of the radius of the circle\n");
	scanf("%f", &rad1);
	return rad1;
}

float computearea(float rad)
{
	float area;
	area = PI * rad * rad;
	return area;
}

float computecir(float rad)
{
	float cir;
	cir = 2* PI * rad;
	return cir;
}

void output(float area, float cir, float rad)
{
	printf("The Area of the circle of radius %f is %f", rad, area);
	printf("The Circumference of the circle of radius %f is %f", rad, cir);
}

int main()
{
	float rad, area, cir;
	rad = input();
	area = computearea(rad);
	cir = computecir(rad);
	output(area, cir, rad);
	return 0;
}
