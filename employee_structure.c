#include<stdio.h>

struct emp
{
	int id;
	char name[20];
	int sal;
};

int main()
{
	int i,j;
	struct emp E[5];
	printf("Enter the details of each employee\n");
	for(i = 0;i<5;i++)
	{
		printf("%d employee : \n", i + 1);
		printf("Name : ");
		scanf("%s",E[i].name);
		printf("ID : ");
		scanf("%d",&E[i].id);
		printf("Salary : ");
		scanf("%d",&E[i].sal);
	}
	printf("The details of each employee are\n");
	for(i = 0;i<5;i++)
	{
		printf("%d employee : \n",i + 1);
		printf("Name : ");
		puts(E[i].name);
		printf("ID : %d\n",E[i].id);
		printf("Salary : %d\n",E[i].sal);
	}
	return 0;
}