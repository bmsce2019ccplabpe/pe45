#include<stdio.h>

int read_array(int arr[100], int n)
{
	printf("Enter the number of elements in the array\n");
	scanf("%d", &n);
	printf("Enter the elements\n");
	for(int i = 0;i<n;i++)
	{
		scanf("%d", &arr[i]);
	}
	return n;
}

int find_largest(int arr[100], int n)
{
	int large_pos = 0;
	for(int i = 0;i<n;i++)
	{
		if(arr[i]>arr[large_pos])	
		{
			large_pos = i;
		}	
	}
	return large_pos;
}

int find_smallest(int arr[100], int n)
{
	int small_pos = 0;
	for(int i = 0;i<n;i++)
	{
		if(arr[i]<arr[small_pos])	
		{
			small_pos = i;
		}	
	}
	return small_pos;
}

void interchange(int arr[], int large, int small)
{
	int temp = arr[large];
	arr[large] = arr[small];
	arr[small] = temp;
}

void print_array(int arr[], int n)
{
	printf("The elements in the array after interchanging smallest and largest numbers are \n");
	for(int i = 0;i<n;i++)
	{
		printf("%d\n",arr[i]);
			
	}
}

int main()
{
	int arr[100], large_pos, small_pos, n;
	n = read_array(arr,n);
	large_pos = find_largest(arr,n);
	small_pos = find_smallest(arr,n);
	printf("The smallest and the largest numbers in the array are %d and %d\n ", arr[small_pos], arr[large_pos]);
	interchange(arr, large_pos, small_pos);
	print_array(arr,n);
}