#include<stdio.h>
#include<math.h>

float input(float a)
{
	scanf("%f", &a);
	return a;
}

float compute(float x1, float x2, float y1, float y2, float d)
{
	d = sqrt(((x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2)));
	return d;
} 

void output(float d)
{
	printf("The distance between two points is %f", d);
}

int main()
{
	float x1, y1, x2, y2, d;
	printf("Enter the value of x1, y1, x2, y2\n");
	x1 = input(x1);
	x2 = input(x2);
	y1 = input(y1);
	y2 = input(y2);
	d = compute(x1, x2, y1, y2, d);
	output(d);
	return 0;
}