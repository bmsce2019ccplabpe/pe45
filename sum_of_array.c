#include<stdio.h>

int input(int a[100], int n)
{
	for(int i = 0;i<n;i++)
	{
		scanf("%d",&a[i]);
	}
}

int compute(int a[100], int n)
{
	int sum = 0;
	for(int i = 0;i<n;i++)
	{
		sum = sum + a[i];
	}
	return sum;
}

void output(int sum)
{
	printf("The sum of n digits in an array is %d",sum);
}

int main()
{
	int x[100], n, sum = 0;;
	printf("Enter the number of numbers needed for the array\n");
	scanf("%d",&n);
	printf("Enter the numbers\n");
	input(x, n);
	sum = compute(x,n);
	output(sum);
}