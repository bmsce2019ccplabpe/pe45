#include<stdio.h>

int input(int a[100], int n)
{
	for(int i = 0;i<n;i++)
	{	printf("%d:",i + 1);
		scanf("%d",&a[i]);
		printf("\n");
	}
}


int main()
{
	int n, num[100], den[100], mul_d = 1, sum_n = 0,gcd;
	printf("How many fractions do you want to add\n");
	scanf("%d",&n);
	printf("Enter the numerators of the fraction\n");
	input(num,n);
	printf("Enter the denominators of the fraction\n");
	input(den,n);
	for(int i = 0;i<n;i++)
	{
		mul_d = mul_d * den[i];
	}
	for(int i = 0;i<n;i++)
	{
		sum_n += (mul_d * num[i])/den[i];
	}	
	for(int i = 1;i<=mul_d && i<=sum_n;i++)
	{
		if(mul_d%i==0 && sum_n%i==0)
		{
			gcd = i;
		}
	}
	mul_d = mul_d/gcd;
	sum_n = sum_n/gcd;
	printf("The sum of n fractions is %d / %d \n", sum_n, mul_d);
	return 0;
}