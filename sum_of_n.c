#include<stdio.h>

int input()
{
	int var;
	scanf("%d", &var);
	return var;
}

int compute(int var)
{
	int res = 0, i;
	for(i = 0;i<=var;i++)
	{
		res= res + i;
	}
	return res;
}

void output(int res)
{
	printf("The Sum of first n natural numbers is %d\n", res);
}

int main()
{
	int var, res;
	printf("Input the number\n");
	var = input();
	res = compute(var);
	output(res);
	return 0;
}