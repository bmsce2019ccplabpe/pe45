#include<stdio.h>

int main()
{
	int i;
	printf("The Years that are leap years between 1980 and 2020 are\n");
	for(i = 1980;i<=2020; i++)
	{ 
		if(i%4 == 0)
		{
			if(i%100 == 0)
			{
				if(i%400 == 0)
				{
					printf("%d\n", i);
				}
				else
				{
					continue;
				}
			}
			else
			{
				printf("%d\n", i);
			}			
		}
		else
		{
			continue;
		}
	}
	return 0;
}