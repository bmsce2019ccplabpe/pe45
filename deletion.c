#include<stdio.h>

int input(int arr[])
{
	int n;
	printf("Enter the number of elements in the array\n");
	scanf("%d",&n);
	printf("Enter the elements in the array\n");
	for(int i = 0;i<n;i++)
	{
		scanf("%d", &arr[i]);
	}
	return n;
}

int compute(int arr[], int n)
{
	int s, flag = 0;
	printf("Enter the element that you want to delete\n");
	scanf("%d",&s);
	for(int i = 0;i<n;i++)
	{
		if(arr[i] == s)
		{
			flag = 1;			
			for(int j = i;j<n;j++)
			{
				arr[j] = arr[j + 1];
			}
			n--;
		}
	} 
	if(!flag)
	{
		printf("The array element could not be found\n");
	}
	return n;
}

void output(int arr[], int n)
{
	printf("The elements are \n");
	for(int i = 0;i<n;i++)
	{
		printf("%d\n",arr[i]); 
	}
}

int main()
{
	int arr[100], n;
	n = input(arr);	
	n = compute(arr,n);
	output(arr,n);
}