#include<stdio.h>

int main()
{
	int arr[30], i, n, sea, flag = 0;
	printf("Enter the number of elements\n");
	scanf("%d",&n);
	printf("Enter the array\n");
	for(i = 0;i<n;i++)
	{
		scanf("%d",&arr[i]);
	}
	printf("Enter the element to be searched\n");
	scanf("%d",&sea);
	for(i = 0;i<n;i++)
	{
		if(sea == arr[i])
		{
			printf("The element has been found at %d position\n", i + 1);
			flag++;
			return 0;
		}
	}
	if(!flag)
	{
		printf("The searched element could not be found\n");
		return 0;
	}
}