#include<stdio.h>
#include<math.h>

struct point
{
	float x, y;
};
float input()
{
	float a;
	scanf("%f", &a);
	return a;
}

float compute(struct point p1, struct point p2)
{
	float d = sqrt(((p1.x - p2.x)*(p1.x - p2.x) + (p1.y - p2.y)*(p1.y - p2.y)));
	return d;
} 

void output(float d)
{
	printf("The distance between two points is %f", d);
}

int main()
{
	struct point p1, p2;
	float d;
	printf("Enter the values for point 1\n");
	p1.x = input();
	p1.y = input();
	printf("Enter the values for point 1\n");
	p2.x = input();
	p2.y = input();
	d = compute(p1, p2);
	output(d);
	return 0;
}