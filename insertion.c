#include<stdio.h>

int input(int arr[])
{
	int n;
	printf("Enter the number of elements in the array\n");
	scanf("%d",&n);
	printf("Enter the elements in the array\n");
	for(int i = 0;i<n;i++)
	{
		scanf("%d", &arr[i]);
	}
	return n;
}

int compute(int arr[], int n)
{
	int s, pos;
	printf("Enter the element that you want to insert and the position it has to be inserted in\n");
	scanf("%d%d",&s, &pos);
	for(int i = n;i>pos;i--)
	{
		arr[i]= arr[i - 1];
	} 
	arr[pos] = s;
	n++;
	return n;
}

void output(int arr[], int n)
{
	printf("The elements are \n");
	for(int i = 0;i<n;i++)
	{
		printf("%d\n",arr[i]); 
	}
}

int main()
{
	int arr[100], n;
	n = input(arr);	
	n = compute(arr,n);
	output(arr,n);
}