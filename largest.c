#include<stdio.h>

float choice(float m1, float m2, float m3)
{
	if(m1>m2)
	{
		if(m1>m3)
		{
			return m1;
		}
		else
		{
			return m3;
		}
	}
	if(m1<m2)
	{
		if(m2>m3)
		{
			return m2;
		}	
		else
		{
			return m3;
		}
	}
}

int main()
{
	float marks1, marks2, marks3, high;
	printf("Please enter your marks\n");
	scanf("%f%f%f", &marks1, &marks2, &marks3);
	high = choice(marks1, marks2, marks3);
	printf("The highest marks between the three given is %f", high);
	return 0; 		
}