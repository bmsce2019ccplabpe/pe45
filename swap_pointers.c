#include<stdio.h>

void swap(int *a, int *b)
{
	int temp = *a;
	*a = *b;
	*b = temp;
}

int main()
{
	int *p1,*p2;
	printf("Enter the values for the first pointer\n");
	scanf("%d",&p1);
	printf("Enter the values for the second pointer\n");
	scanf("%d",&p2);
	swap(&p1,&p2);
	printf("The pointers after swapping are %d and %d\n", p1, p2);
	return 0;
}
